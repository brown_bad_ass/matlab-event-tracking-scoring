

% sql = 'SELECT * from trip WHERE firmwareversion = ''4.0.15.2''' ;
sql = 'select distinct(trip.deviceserialnumber),trip.vehicleid from trip WHERE trip.starttime > ''2016-05-01'' AND (trip.INDEX > 10)';

curstrip = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');

curstrip = fetch(curstrip);
IDDongleVehicle = (curstrip.Data);  

[r,c] = size(IDDongleVehicle);

VehDetails = cell(r,14);

for i=1:r
   
    temp = strcmp('null',IDDongleVehicle{i,1});
    
    if (temp == 0)
      
        VehID = char(IDDongleVehicle{i,2});
        
        sql = ['select vehicle.make,vehicle.model,vehicle.year,vehicle.obdprotocol,vehicle.obdconnectiondetails,vehicle.vehiclespeedsensorecucount,vehicle.rpmecucount,vehicle.massairpressureecucount,vehicle.massairflowecucount,vehicle.fueltype,vehicle."engineCapacity","user".firstname,"user".lastname,"user".email from vehicle, "user" WHERE vehicle.userid = "user".id AND (vehicle.id = ''',VehID,''')']; 
        
        curs = exec(conn,sql);
        setdbprefs('DataReturnFormat','cellarray');

        curs = fetch(curs);
        VehDetails(i,:) = (curs.Data);  
        
    end
    
end