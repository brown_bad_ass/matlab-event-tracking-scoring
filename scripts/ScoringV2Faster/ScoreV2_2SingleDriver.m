
% Score V2.2

sql = ['select distinct(event.tripid),sum(event."peakValue"),avg(trip.distance),avg(trip."accelerationScore"),count(*),avg(trip."brakingScore")'... 
' from event inner join trip on event.tripid = trip.id '... 
' where event.TIMESTAMP > ''2016-07-01'' AND trip.firmwareversion >= ''4.0.18.0'' '...  
' AND (trip."accelerationScore" > 0) AND (trip."brakingScore" > 0) '... 
' AND (trip."accelerationScore" < 100) AND (trip."brakingScore" < 100) '... 
' AND event.TYPE = ''1'' AND event."peakValue" > 0.2 AND trip.deviceserialnumber = ''DMX152620107'' GROUP BY event.tripid'];

cursEvtV2 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2 = fetch(cursEvtV2);
EventsV2 = (cursEvtV2.Data); 

% [row,col] = size(EventsV2);
% 
% for i=1:row
%     EventsV2{i,1} = char(EventsV2{i,1});
% end
figure
histogram(cell2mat(EventsV2(:,4)),50)
title('Acceleration Scores Histogram V1 ');
ylabel('Frequency');
xlabel('Scores');

[r,c] = size(EventsV2);
SumOfPeak = cell2mat(EventsV2(:,2));
Dist = cell2mat(EventsV2(:,3));
PeakPerKm = SumOfPeak./Dist;

DistPk = Dist./SumOfPeak;
% logdistpk = (log(Dist+1)./log(SumOfPeak+1)).^0.5;
logpkdist = (log(SumOfPeak+1)./log(Dist+1)).^0.5;

Badness = logpkdist*30;

scalar = ones(r,1)*100; % Mean shifter
scores = minus(scalar,Badness);
figure
histogram(scores,50)
title('Acceleration Scores Histogram V2.2 Before CLipping');
ylabel('Frequency');
xlabel('Scores');

