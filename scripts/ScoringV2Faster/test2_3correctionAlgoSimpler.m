

%test V2.3 correction algo simpler attempt

[NumEvt,ColEvt] = size(temp);
GrpPeak = 0;
sumPeak = 0;
StartGroup = 0;

EvtCnt = 1;
PeakArr = zeros(1,1);
j = 1;

while EvtCnt < NumEvt

    if StartGroup == 0
        
        TimeDiff = 86400*(datenum(temp{EvtCnt+1,3},'yyyy-mm-dd HH:MM:SS')-datenum(temp{EvtCnt,3},'yyyy-mm-dd HH:MM:SS'));
        
        if TimeDiff <= 3.01
            
           StartGroup = 1;
           StartGruopTimeStamp = temp{EvtCnt,3};
           GrpPeak = temp{EvtCnt,2};
           
        else
            
           
           PeakArr(j,1) = temp{EvtCnt,2};
           j = j+1;
           EvtCnt = EvtCnt+1;
           
           
        end
        
    %start of group   
    else 
        TimeDiffGrp = 86400*(datenum(temp{EvtCnt+1,3},'yyyy-mm-dd HH:MM:SS')-datenum(StartGruopTimeStamp,'yyyy-mm-dd HH:MM:SS'));
        
        
        
        if TimeDiffGrp <= 3.01
           
            if GrpPeak < temp{EvtCnt+1,2};
                
                GrpPeak = temp{EvtCnt+1,2};
                
            end
            
            EvtCnt = EvtCnt+1;
            
        else 
            EvtCnt = EvtCnt+1;
            PeakArr(j,1) = GrpPeak;
            j = j+1;
            StartGroup = 0;
             
        end
        
    end
    
end