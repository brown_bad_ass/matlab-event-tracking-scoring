
%Score 2.3 Corrected Single Trip

sql = ['select event.tripid,event."peakValue",event.TIMESTAMP,trip.distance'... 
' from event inner join trip on event.tripid = trip.id '... 
' where event.TIMESTAMP > ''2016-09-01'' AND trip.firmwareversion >= ''4.0.19.1'' '...  
' AND event.TYPE = ''1'' AND event.tripid = ''5272aed8-1bb8-07d8-1695-ca4751109892'' ORDER BY event.TIMESTAMP ASC '];

cursEvtV2_3 = exec(conn_Staging,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2_3 = fetch(cursEvtV2_3);
EventsV2_3 = (cursEvtV2_3.Data); 

[row,col] = size(EventsV2_3);

for i=1:row
    
    EventsV2_3{i,1} = char(EventsV2_3{i,1});
    
end

[NumOfEvts,c] = size(EventsV2_3);

TripMultiplier = 12.3;

if NumOfEvts > 1
    
    [ScoreV2_3Before,ScoreV2_3After,PeakArr] = GroupEvtsAndScore(EventsV2_3,TripMultiplier);

else

    pkorig = EventsV2_3{1,2};
    pkcorr = EventsV2_3{1,2};
    dist =  EventsV2_3{1,4};
    
    [ScoreV2_3Before,ScoreV2_3After,PeakArr] = ScoreV2_3Corrected(pkorig,pkcorr, dist,TripMultiplier);

end


