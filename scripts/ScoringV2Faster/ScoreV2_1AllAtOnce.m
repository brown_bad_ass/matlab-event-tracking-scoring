
NumOfEvts = cell2mat(EventsV2(:,5));
% lognumevts = log(cell2mat(EventsV2(:,5))+1.1);
avgofpeak = cell2mat(EventsV2(:,2));

[r,c] = size(EventsV2);
DistComp = zeros(r,1);

% maxpeak = max(avgofpeak);
maxpeak = 0.6;

Distance = cell2mat(EventsV2(:,3));

for index = 1:r
    if Distance(index) < 20
         DistComp(index) = Distance(index)*0.4;
    elseif Distance(index) >= 20 && Distance(index) < 40    
         DistComp(index) = Distance(index)*0.15;
    else
         DistComp(index) = Distance(index)*0.1; 
    end
        
end

% DistComp = log(DistComp+1);


scalar = ones(r,1)*(maxpeak+0.01);
normalizedpeak = minus(scalar,avgofpeak);

% for index = 1:r
%     if lognumevts(index) > DistComp(index)
%         lognumevts(index) = lognumevts(index) - DistComp(index);
% 
%     end
% end
CompEvts = NumOfEvts - DistComp;

for index = 1:r
    
    if CompEvts(index) < 1 
        CompEvts(index) = 1.1;
    end
    
end



lognumevts = log(CompEvts);

lognumevts = log(NumOfEvts+0.1) - log(DistComp);
distoverpeak = lognumevts./normalizedpeak;
distoverpeak = distoverpeak.^1.5;
avg = nanmean(distoverpeak);
stdd = nanstd(distoverpeak);

% figure
% histogram(distoverpeak,100)

% constant = max(distoverpeak)-min(distoverpeak);
scalar = ones(r,1)*100; % Mean shifter
scores = minus(scalar,distoverpeak);

for index = 1:r
    
    if scores(index) < 0
        scores(index) = 0;
    end
    
end

stdscores = std(scores);
avgscores = mean(scores);
maxscore = max(scores);

figure
histogram(scores,200)
% title('Accel Histogram MaxPeak: 0.6 Pow: 1.7');
title('Braking Scores Histogram V2.2');
ylabel('Frequency');
xlabel('Scores');
