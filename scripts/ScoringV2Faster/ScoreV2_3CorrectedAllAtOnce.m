

%ScoreV2_3CorrectedAllAtOnce

% FindNumOfEvnt2_3

% % Unit Test 
%   startEvtInd = 1;
% % 
% %  Evtdata = EventsV2(startEvtInd:EvtCountTrip(1,2),:);
% % 
% [AccScore,AccScoreCorr,PeakAr] = GroupEvtsAndScore(Evtdata);

%startEvtInd = EvtCountTrip(1,2);
% 
%Evtdata = EventsV2(startEvtInd+1:EvtCountTrip(2,2),:);
% 
%[AccScore,AccScoreCorr] = GroupEvtsAndScore(Evtdata);


startEvtInd = 0;
Scores = zeros(1,2);
j = 1;

for TripIndex = 1:NumTrips
   
    if EvtCountTrip(TripIndex,1) > 1
        
        Evtdata = EventsV2(startEvtInd+1:EvtCountTrip(TripIndex,2),:);
        startEvtInd  = EvtCountTrip(TripIndex,2);
        [AccScore,AccScoreCorr,PkCorrected]  = GroupEvtsAndScore(Evtdata);
        Scores(j,1) = AccScore;
        Scores(j,2) = AccScoreCorr;
        j = j+1;
        
    else
        Evtdata = EventsV2(startEvtInd+1,:);
        PK = Evtdata{1,2};
        DIST = Evtdata{1,4};
        
        [AccScore,AccScoreCorr,PkCorrected]  = ScoreV2_3Corrected(PK,PK, DIST);
        Scores(j,1) = AccScore;
        Scores(j,2) = AccScoreCorr;
        j = j+1;
        
        startEvtInd  = EvtCountTrip(TripIndex,2);
       
    end
    
end

figure 
histogram(Scores(:,1),100)
title('Braking Scores V 2.3 Before Correction');
ylabel('Frequency');
xlabel('Scores');
figure 
histogram(Scores(:,2),100)
title('Braking Scores V 2.3 After Correction');
ylabel('Frequency');
xlabel('Scores');










