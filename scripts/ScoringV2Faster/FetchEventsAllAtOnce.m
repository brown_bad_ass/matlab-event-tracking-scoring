
%Fetch Events all at Once 
%EventsV2 = cell({});
 sql = ['select event.tripid,event."peakValue",event.TYPE,event.curveshape,trip.distance,trip."averageSpeed",trip."accelerationScore",trip."brakingScore"'... 
' from event inner join trip on event.tripid = trip.id '... 
' where event.TIMESTAMP > ''2016-09-12'' AND trip.firmwareversion >= ''4.0.19.1'' '...  
' AND (trip."accelerationScore" > 0) AND (trip."brakingScore" > 0) '... 
' AND (trip."accelerationScore" < 100) AND (trip."brakingScore" < 100) '... 
' AND event.TYPE < ''3'' ORDER BY event.tripid'];

cursEvtV2 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2 = fetch(cursEvtV2);
EventsV2 = (cursEvtV2.Data); 

[row,col] = size(EventsV2);

for i=1:row
    
    EventsV2{i,1} = char(EventsV2{i,1});
    
end