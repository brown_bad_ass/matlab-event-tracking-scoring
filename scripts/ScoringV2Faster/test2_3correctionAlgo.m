

%test V2.3 correction algo

[NumEvt,ColEvt] = size(temp);

sumPeak = 0;
StartGroup = 0;

EvtCnt = 1;

while EvtCnt < NumEvt
    
    TimeDiff = 86400*(datenum(temp{EvtCnt+1,3},'yyyy-mm-dd HH:MM:SS')-datenum(temp{EvtCnt,3},'yyyy-mm-dd HH:MM:SS'));
    CurrentPeak = temp{EvtCnt,2};
    
    if TimeDiff <= 3
        
        StartGroup = 1;
        StartGruopTimeStamp = temp{EvtCnt,3};
        GrpInd = 1;
        GrpPeak = temp{EvtCnt,2};
        
        while StartGroup == 1
            
            if EvtCnt+GrpInd < NumEvt-1
                
                TimeDiffGroup = 86400*(datenum(temp{EvtCnt+GrpInd,3},'yyyy-mm-dd HH:MM:SS')-datenum(StartGruopTimeStamp,'yyyy-mm-dd HH:MM:SS'));

                if TimeDiffGroup <= 3

                    if GrpPeak < temp{EvtCnt+GrpInd,2}

                        GrpPeak = temp{EvtCnt+GrpInd,2};

                    end

                    GrpInd = GrpInd+1;

                else
                    sumPeak = sumPeak+GrpPeak;
                    StartGroup = 0;
                    EvtCnt = EvtCnt+GrpInd;
                end
            
            else
                
            
            end
            
        end
        
    else
        sumPeak = sumPeak+CurrentPeak;
        EvtCnt = EvtCnt+1;
        
        
        
    end
    
end