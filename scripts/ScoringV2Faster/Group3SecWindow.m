
function [GrpCell] = Group3SecWindow(data)

GrpInd = 1;
GrpMem = 1;
GrpCell = cell({});

[evtr,evtc] = size(data);

StartGrpTimeStamp = data{1,3};

for evtInd = 1:evtr
    
    TimeDiff =  86400*(datenum(data{evtInd,3},'yyyy-mm-dd HH:MM:SS')-datenum(StartGrpTimeStamp,'yyyy-mm-dd HH:MM:SS'));
    
    if TimeDiff < 3.01
        
        GrpCell{GrpInd,1}(GrpMem,1) = data{evtInd,2};
        GrpMem = GrpMem+1;
    else
        GrpMem = 1;
        GrpInd = GrpInd+1;
        StartGrpTimeStamp = data{evtInd,3};
        GrpCell{GrpInd,1}(GrpMem,1) = data{evtInd,2};  
        GrpMem = GrpMem+1;
        
    end
    
    
    
end

end
