

CurrentTripID = EventsV2{1,1};

NumOfEvtCurrentTrip = 0;
TripCount = 1;
EvtCountTrip = zeros(1,2);


for evtidx = 1:row
   
   TripID = EventsV2{evtidx,1};
   
   if strcmp(TripID,CurrentTripID) == 1
       NumOfEvtCurrentTrip = NumOfEvtCurrentTrip+1;
   else
       EvtCountTrip(TripCount,1) = NumOfEvtCurrentTrip;
       TripCount = TripCount+1;
       CurrentTripID = TripID;
       NumOfEvtCurrentTrip = 1;
   end
    
end

[NumTrips,col] = size(EvtCountTrip);

sumInd = 0;
TripCount = 1;

for TripCount = 1:NumTrips
    
    sumInd = sumInd+EvtCountTrip(TripCount,1) ;
    EvtCountTrip(TripCount,2) = sumInd;
    
end
