
% V 2.2 
% sql = ['select sum(event."peakValue"),count(*),avg(trip.distance),avg(event."peakValue") from event inner join trip on event.tripid = trip.id where event.TYPE = ''1'' AND event.tripid = ''' ,TripID,''''];
% 
% cursEvtV2_2 = exec(conn,sql);
% setdbprefs('DataReturnFormat','cellarray');
% cursEvtV2_2= fetch(cursEvtV2_2);
% EventsV2_2 = (cursEvtV2_2.Data); 
% 
% logdist = log(EventsV2_2{1,3}+1);
% 
% logsumpk = log(EventsV2_2{1,1}+1);
% 
% logPeakOverDist = (logsumpk/logdist)^0.5;
% 
% ScoreV2_2 = 100-30*logPeakOverDist;

% V 2.3 Accel
% TripID = 'ef67308f-3829-8bb5-c45f-7221753d95ef';
% sql = ['select sum(event."peakValue"),count(*),avg(trip.distance),avg(trip."accelerationScore"),avg(trip."brakingScore"),avg(trip.litres) from event inner join trip on event.tripid = trip.id where event.TYPE = ''1'' AND event.tripid = ''' ,TripID,''''];
% 
% cursEvtV2_2 = exec(conn,sql);
% setdbprefs('DataReturnFormat','cellarray');
% cursEvtV2_2= fetch(cursEvtV2_2);
% EventsV2_2 = (cursEvtV2_2.Data); 
% 
% AccScoreV1 = EventsV2_2{1,4};
% BrkScoreV1 = EventsV2_2{1,5};
% TripDist   = EventsV2_2{1,3};
% TripLit    = EventsV2_2{1,6};
% 
% logdist = log(EventsV2_2{1,3}+1);
% 
% logsumpk = log(EventsV2_2{1,1}+1)/log(1.5);
% 
% logPeakOverDist = (logsumpk/logdist)^0.5;
% 
% AccScoreV2_3 = 100-26*logPeakOverDist;
% AccScoreV2_3 = AccScoreV2_3+10;
% 
% % V 2.3 Braking  
% sql = ['select sum(event."peakValue"),count(*),avg(trip.distance),avg(trip."accelerationScore"),avg(trip."brakingScore"),avg(trip.litres) from event inner join trip on event.tripid = trip.id where event.TYPE = ''2'' AND event.tripid = ''' ,TripID,''''];
% 
% cursEvtV2_2 = exec(conn,sql);
% setdbprefs('DataReturnFormat','cellarray');
% cursEvtV2_2= fetch(cursEvtV2_2);
% EventsV2_2 = (cursEvtV2_2.Data); 
% 
% % AccScoreV1 = EventsV2_2{1,4};
% % BrkScoreV1 = EventsV2_2{1,5};
% % TripDist   = EventsV2_2{1,3};
% % TripLit    = EventsV2_2{1,6};
% 
% logdist = log(EventsV2_2{1,3}+1);
% 
% logsumpk = log(EventsV2_2{1,1}+1)/log(1.5);
% 
% logPeakOverDist = (logsumpk/logdist)^0.5;
% 
% BrkScoreV2_3 = 100-26*logPeakOverDist;
% BrkScoreV2_3 = BrkScoreV2_3+10;

% V 2.4 Accel
TripID = 'ef67308f-3829-8bb5-c45f-7221753d95ef';
sql = ['select sum(event."peakValue"),count(*),avg(trip.distance),avg(trip."accelerationScore"),avg(trip."brakingScore"),avg(trip.litres) from event inner join trip on event.tripid = trip.id where event.TYPE = ''1'' AND event.tripid = ''' ,TripID,''''];

cursEvtV2_2 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2_2= fetch(cursEvtV2_2);
EventsV2_2 = (cursEvtV2_2.Data); 

AccScoreV1 = EventsV2_2{1,4};
BrkScoreV1 = EventsV2_2{1,5};
TripDist   = EventsV2_2{1,3};
TripLit    = EventsV2_2{1,6};



sumpk = EventsV2_2{1,1};

PeakOverDist = (sumpk/TripDist)^0.5;

AccScoreV2_3 = 100-40*PeakOverDist;
AccScoreV2_3 = AccScoreV2_3-1;

% V 2.4 Braking  
sql = ['select sum(event."peakValue"),count(*),avg(trip.distance),avg(trip."accelerationScore"),avg(trip."brakingScore"),avg(trip.litres) from event inner join trip on event.tripid = trip.id where event.TYPE = ''2'' AND event.tripid = ''' ,TripID,''''];

cursEvtV2_2 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2_2= fetch(cursEvtV2_2);
EventsV2_2 = (cursEvtV2_2.Data); 


sumpk = EventsV2_2{1,1};

PeakOverDist = (sumpk/TripDist)^0.5;

BrkScoreV2_3 = 100-40*PeakOverDist;
BrkScoreV2_3 = BrkScoreV2_3-1;

Result = zeros(1,6);

Result(1) = AccScoreV1;
Result(2) = AccScoreV2_3;

Result(3) = BrkScoreV1;
Result(4) = BrkScoreV2_3;

Result(5) = TripDist;
Result(6) = TripLit;


