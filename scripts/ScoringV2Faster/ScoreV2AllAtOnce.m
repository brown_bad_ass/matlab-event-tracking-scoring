

% Score V2 All at Once
% COnstants and Weights

AccPen = [3 4 5]/1000;
BrkPen = [5 8 12]/1000;%Milli sec to Seconds


%Variables initialization
[EventR,EventC] = size(EventsV2);
ScoresV2 = cell(1,13);
j = 1;
k = 1;
bk = 1;
m = 1;
AccBinArr = zeros(1,4);
TopAccScore = zeros(1,10);

CurrentTripID = EventsV2{1,1};
CurrentAccScoreV2 = 255;
CurrentBrkScoreV2 = 255;

for idx = 1:EventR
    
    TripID = EventsV2{idx,1};
    Dist   = EventsV2{idx,5};
    AvgSpd = EventsV2{idx,6};
    Peak   = EventsV2{idx,2};
    
%     AvgSpeedScaling = 2;
% 
%     if AvgSpd < 30
%         AvgSpeedScaling = 1.1;
%     end
% 
%     if (AvgSpd > 40) && (AvgSpd <= 60)
%         AvgSpeedScaling = 4;
%     end
% 
%     if AvgSpd > 60
%         AvgSpeedScaling = 6;
%     end

    AvgSpeedScaling = 2+0.04*AvgSpd;

    if AvgSpd < 30
        AvgSpeedScaling = 1.1+0.035*AvgSpd;
    end

    if (AvgSpd > 40) && (AvgSpd <= 60)
        AvgSpeedScaling = 4+0.02*AvgSpd;
    end

    if AvgSpd > 60
        AvgSpeedScaling = 6+0.01*AvgSpd;
    end


    
    Bins = ParseDuration(EventsV2{idx,4});
    Bins = Bins/Dist; % Normalizing BIn duration
    
    if strcmp(TripID,CurrentTripID) == 1
           %Calculate Accel Score
        if strcmp(EventsV2{idx,3},'1') == 1
            
            AccBinArr(k,1:3) = Bins; % Normalized Duration
            AccBinArr(k,4) = Peak;
            
            CurrentAccScoreV2 = CurrentAccScoreV2-(0.94^k)*(AvgSpeedScaling*(AccPen(1)*Bins(1)+AccPen(2)*Bins(2)+AccPen(3)*Bins(3))*(1.05+Peak)^7 );
            k = k+1;
        else
            %Calculate Braking Score
            CurrentBrkScoreV2 = CurrentBrkScoreV2-(0.945^bk)*(AvgSpeedScaling*(BrkPen(1)*Bins(1)+BrkPen(2)*Bins(2)+BrkPen(3)*Bins(3))*(1.02+Peak)^4 );
            bk = bk+1;
        end 
    
    else
        if CurrentAccScoreV2 < 0
            CurrentAccScoreV2 = 0;
        end
        
        if CurrentBrkScoreV2 < 0
            CurrentBrkScoreV2 = 0;
        end
        
        ScoresV2{j,1} = CurrentTripID;
        
        ScoresV2{j,2} = CurrentAccScoreV2/255*100;
        ScoresV2{j,3} = CurrentBrkScoreV2/255*100;
        
        ScoresV2{j,4} = EventsV2{idx-1,7};    % V1 acc score
        ScoresV2{j,5} = EventsV2{idx-1,8};    % V1 brk score
         
        ScoresV2{j,6} = EventsV2{idx-1,5};    % Distance
        ScoresV2{j,7} = EventsV2{idx-1,6};    % Avg Speed
        
        ScoresV2{j,8}  = sum(AccBinArr(:,1)); % Sum of Bin 1 duration
        ScoresV2{j,9}  = sum(AccBinArr(:,2)); % Sum of Bin 2 duration
        ScoresV2{j,10} = sum(AccBinArr(:,3)); % Sum of Bin 3 duration
        ScoresV2{j,11} = sum(AccBinArr(:,4)); % Sum of Peak
        ScoresV2{j,12} = mean(AccBinArr(:,4));% Avg of Peak
        ScoresV2{j,13} = k-1;                 % Num of accel events 
        
        if ScoresV2{j,2} > 90
            
            TopAccScore(m,1:10) = cell2mat(ScoresV2(j,2:11));
            m = m+1;
        
        end
        k = 1;% re initialize Bin duration array counter
        bk = 1;
        AccBinArr = zeros(1,4);
        j = j+1;
        
        CurrentTripID = TripID;
        CurrentAccScoreV2 = 255;
        CurrentBrkScoreV2 = 255;
        %Need to calculate score for first event of a new tripID
        
            %Calculate Accel Score
        if strcmp(EventsV2{idx,3},'1') == 1 
            AccBinArr(k,1:3) = Bins; % Normalized Duration
            AccBinArr(k,4) = Peak;
           
            CurrentAccScoreV2 = CurrentAccScoreV2-(0.945^k)*(AvgSpeedScaling*(AccPen(1)*Bins(1)+AccPen(2)*Bins(2)+AccPen(3)*Bins(3))*(1.05+Peak)^5 );
            k = k+1;
        else
            %Calculate Braking Score
            CurrentBrkScoreV2 = CurrentBrkScoreV2-(0.95^bk)*(AvgSpeedScaling*(BrkPen(1)*Bins(1)+BrkPen(2)*Bins(2)+BrkPen(3)*Bins(3))*(1.02+Peak)^4 );
            bk = bk+1;
           
        end 
        
    end
    
    
    
end
AvgAccScoreV1 = nanmean(cell2mat(ScoresV2(:,4)));
StdAccScoreV1 = nanstd(cell2mat(ScoresV2(:,4)));
AvgAccScoreV2 = nanmean(cell2mat(ScoresV2(:,2)));
StdAccScoreV2 = nanstd(cell2mat(ScoresV2(:,2)));

figure
histogram(cell2mat(ScoresV2(:,2)),100);
title('Accel V2 scores');
ylabel('Occurences');
xlabel('Scores');
figure
histogram(cell2mat(ScoresV2(:,4)),100);
title('Accel V1 scores');
ylabel('Occurences');
xlabel('Scores');
% figure
% histogram(cell2mat(ScoresV2(:,3)),100);
% title('Braking V2 scores');
% ylabel('Occurences');
% xlabel('Scores');
% figure
% histogram(cell2mat(ScoresV2(:,5)),100);
% title('Braking  V1 scores');
% ylabel('Occurences');
% xlabel('Scores');
