

function [ScoreO,ScoreC,PeakArr] = GroupEvtsAndScore(temp,tripmul)

[NumEvt,ColEvt] = size(temp);
PeakArrOrig = cell2mat(temp(:,2));
Distance = temp{1,4};
GrpPeak = 0;

StartGroup = 0;

EvtCnt = 1;
PeakArr = zeros(1,1);
j = 1;

StartGruopTimeStamp = cell({});

while EvtCnt < NumEvt

    
        
    if StartGroup == 0
        
        TimeDiff = 86400*(datenum(temp{EvtCnt+1,3},'yyyy-mm-dd HH:MM:SS')-datenum(temp{EvtCnt,3},'yyyy-mm-dd HH:MM:SS'));
        
        if TimeDiff <= 3.01
            
           StartGroup = 1;
           StartGruopTimeStamp = temp{EvtCnt,3};
           GrpPeak = temp{EvtCnt,2};
           
        else
            
           
           PeakArr(j,1) = temp{EvtCnt,2};
           j = j+1;
           EvtCnt = EvtCnt+1;
           
           
        end
        
    %start of group   
    else 
        TimeDiffGrp = 86400*(datenum(temp{EvtCnt+1,3},'yyyy-mm-dd HH:MM:SS')-datenum(StartGruopTimeStamp,'yyyy-mm-dd HH:MM:SS'));
        
        
        
        if TimeDiffGrp <= 3.01
           
            if GrpPeak < temp{EvtCnt+1,2};
                
                GrpPeak = temp{EvtCnt+1,2};
                
            end
            
            EvtCnt = EvtCnt+1;
            
        else 
            EvtCnt = EvtCnt+1;
            PeakArr(j,1) = GrpPeak;
            j = j+1;
            StartGroup = 0;
             
        end
        
    end
   
end

if (isempty(StartGruopTimeStamp) == 0 && (j > 1))
    
    TimeDiffGrp = 86400*(datenum(temp{NumEvt,3},'yyyy-mm-dd HH:MM:SS')-datenum(StartGruopTimeStamp,'yyyy-mm-dd HH:MM:SS'));
    
    if  TimeDiffGrp <= 3.01
           
            if GrpPeak < temp{NumEvt,2};
                
                GrpPeak = temp{NumEvt,2};
                
            end
            
           PeakArr(j,1) = GrpPeak;
            
     else 
            
           PeakArr(j,1) = temp{NumEvt,2};
            
             
     end
    
end

if (NumEvt == 2)
    
    TimeDiffGrp = 86400*(datenum(temp{NumEvt,3},'yyyy-mm-dd HH:MM:SS')-datenum(temp{NumEvt-1,3},'yyyy-mm-dd HH:MM:SS'));
    
    PeakArr = zeros(1,1);
    
    if  TimeDiffGrp <= 3.01
           
          if temp{NumEvt,2} > temp{NumEvt-1,2}
              PeakArr(1,1) = temp{NumEvt,2};
          else
              PeakArr(1,1) = temp{NumEvt-1,2};
          end
              
            
    else 
            
          PeakArr(1,1) = temp{NumEvt-1,2};
          PeakArr(2,1) = temp{NumEvt,2};
            
             
    end
    
    
end


    [ScoreO,ScoreC,PeakArr] = ScoreV2_3Corrected(PeakArrOrig,PeakArr, Distance,tripmul);
    


end