
function [Bin] = ParseDuration(EventDuration)

Bin = [];
if length(EventDuration) > 70
    
    IndexSemiColon = strfind(EventDuration,':');
    IndexComma = strfind(EventDuration,',');
    
    Bin1 = EventDuration(IndexSemiColon(1)+1:IndexComma(1)-1);
    Bin2 = EventDuration(IndexSemiColon(3)+1:IndexComma(3)-1);
    Bin3 = EventDuration(IndexSemiColon(5)+1:IndexComma(5)-1);
    
    Bin(1) = str2num(Bin1);
    Bin(2) = str2num(Bin2);
    Bin(3) = str2num(Bin3);
    
else
    
    Bin(1) = 0;
    Bin(2) = 0;
    Bin(3) = 0;
    
    
end

end