


sql = 'select avg(event."peakValue"),count(*),avg(trip.distance) from event inner join trip on event.tripid = trip.id where event.TYPE = ''1'' AND event.tripid = ''ef67308f-3829-8bb5-c45f-7221753d95ef''';

cursEvtV2_1 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2_1 = fetch(cursEvtV2_1);
EventsV2_1 = (cursEvtV2_1.Data); 


AvgPeak = EventsV2_1{1,1};
NumOfEvents = EventsV2_1{1,2};
Distance = EventsV2_1{1,3};

if   Distance < 20
         DistComp = Distance*0.4;
elseif Distance >= 20 && Distance < 40    
         DistComp = Distance*0.15;
else
         DistComp = Distance*0.1; 
end


logEvt = log((NumOfEvents-DistComp));

% logEvt = log(NumOfEvents+1.1);
% logEvt = logEvt-1.79;
NormalizedPeak = 0.6 - AvgPeak;
Badness = (logEvt/NormalizedPeak)^1.0;
ScoreV2_1 = 100-Badness;


