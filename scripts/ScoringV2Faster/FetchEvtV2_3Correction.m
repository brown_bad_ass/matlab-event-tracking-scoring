

% Fetch Events 2.3 for correction algorithm

sql = ['select event.tripid,event."peakValue",event.TIMESTAMP,trip.distance'... 
' from event inner join trip on event.tripid = trip.id '... 
' where event.TIMESTAMP > ''2016-10-01'' AND trip.firmwareversion >= ''4.0.19.1'' '...  
' AND (trip."accelerationScore" > 0) AND (trip."brakingScore" > 0) '... 
' AND (trip."accelerationScore" < 100) AND (trip."brakingScore" < 100) '... 
' AND event.TYPE = ''2'' AND event."peakValue" > 0.2 ORDER BY event.tripid ASC, event.TIMESTAMP ASC '];

cursEvtV2 = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');
cursEvtV2 = fetch(cursEvtV2);
EventsV2 = (cursEvtV2.Data); 

[row,col] = size(EventsV2);

for i=1:row
    
    EventsV2{i,1} = char(EventsV2{i,1});
    
end