

%Experiment V2.2
[r,c] = size(EventsV2);
SumOfPeak = cell2mat(EventsV2(:,2));
Dist = cell2mat(EventsV2(:,3));
PeakPerKm = SumOfPeak./Dist;

DistPk = Dist./SumOfPeak;
% 
% logpkdist_pete = (log2(SumOfPeak+1.0)./log(Dist+1.0)).^0.5; % Pete's suggest to use log2 on Sum of peak
% Badness = logpkdist_pete*30;

% logpkdist_fat = (log(SumOfPeak+1.0)./log(Dist+1.0)).^0.5; % V2.2 
% Badness = logpkdist_fat*30;

% logpkdist_thin_exp_dec = (log(SumOfPeak+1)./log(Dist+1)).^0.4; 
% Badness = logpkdist_thin_exp_dec*30;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2 thin exp = 0.4, Distance X 1,Peak * 1');
% ylabel('Frequency');
% xlabel('Scores');
% 
% logpkdist_thin = (log(0.8*SumOfPeak+1)./log(2*Dist+1)).^0.4; 
% Badness = logpkdist_thin*30;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2 thin exp = 0.4, Distance X 2,Peak * 0.8');
% ylabel('Frequency');
% xlabel('Scores');

% logpkdist_thin_exp_inc = (log(1*SumOfPeak+1.01)./log(2*Dist+1.01)).^0.6; 
% Badness = logpkdist_thin_exp_inc*30;

% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2 , Pete suggestion log2');
% ylabel('Frequency');
% xlabel('Scores');

% log_1_5_base = log(SumOfPeak+1.0)/log(1.5);
% logpkdist_base1_5 = (log_1_5_base./log(Dist+1.0)).^0.5; % Sum of peak log base 1.5
% Badness = logpkdist_base1_5*26;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores+10;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak base 1.5, Offset = 10, Multi = 26');
% ylabel('Frequency');
% xlabel('Scores');


% logpkdist_base_e = (log(SumOfPeak+1.0)./log(Dist+1.0)).^0.5; % Sum of peak log base e = V2.2
% Badness = logpkdist_base_e*30;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2 base e');
% ylabel('Frequency');
% xlabel('Scores');
% 
% log_1_5_base = log(SumOfPeak+1.0)/log(1.4);
% logpkdist_base1_5 = (log_1_5_base./log(Dist+1.0)).^0.5; % Sum of peak log base 1.5
% Badness = logpkdist_base1_5*26;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores+10;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak base 1.4, Offset = 10, multi = 26');
% ylabel('Frequency');
% xlabel('Scores');
% 
% 
log_1_5_base = log(SumOfPeak+1.0)/log(1.5);
logpkdist_base1_5 = (log_1_5_base./log(Dist+1.01)).^0.5; % Sum of peak log base 1.5
Badness = logpkdist_base1_5*26;

scalar = ones(r,1)*100; % Mean shifter
scores = minus(scalar,Badness);
scores = scores+10;

for idx = 1:r
    
    if scores(idx) < 0
        scores(idx) = 0;
        
    elseif scores(idx) > 100
        scores(idx) = 100;
    end
    
end
mean_score = mean(scores);
std_score = std(scores);
figure
histogram(scores,200)
title('Braking Scores Histogram V2.3, Sum of peak base 1.5, Offset = 10, Multi = 26');
ylabel('Frequency');
xlabel('Scores');


% logpkdist_base_e = (log(SumOfPeak+1.0)./log(Dist+1.0)).^0.5; % Sum of peak log base e = V2.2
% Badness = logpkdist_base_e*30;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2 base e');
% ylabel('Frequency');
% xlabel('Scores');

% log_1_5_base = log(SumOfPeak+1.0)/log(1.2);
% 
% logpkdist_base1_5 = (log_1_5_base./log(Dist+1.0)).^0.5; % Sum of peak log base 1.5
% Badness = logpkdist_base1_5*32;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores+17;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak base 1.2, Offset = 17, multi = 32');
% ylabel('Frequency');
% xlabel('Scores');




% logpkdist_Peak = (SumOfPeak./log(Dist+1.0)).^0.5; % 
% Badness = logpkdist_Peak*13;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores+5;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak without log, Offset = 5, multi = 13');
% ylabel('Frequency');
% xlabel('Scores');
% 
% logpkdist_Peak = (SumOfPeak./log(Dist+1.0)).^0.5; % 
% Badness = logpkdist_Peak*17;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores+5;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak without log, Offset = 5, multi = 17');
% ylabel('Frequency');
% xlabel('Scores');

% pkdist_Peak = (SumOfPeak./Dist).^0.5; % 
% Badness = pkdist_Peak*10;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores-1;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak without log, distance without log Offset = -1, multi = 10');
% ylabel('Frequency');
% xlabel('Scores');
% 
% pkdist_Peak = (SumOfPeak./Dist).^0.5; % 
% Badness = pkdist_Peak*40;
% 
% scalar = ones(r,1)*100; % Mean shifter
% scores = minus(scalar,Badness);
% scores = scores-1;
% mean_score = mean(scores);
% std_score = std(scores);
% figure
% histogram(scores,200)
% title('Acceleration Scores Histogram V2.2, Sum of peak without log, distance without log Offset = -1, multi = 40');
% ylabel('Frequency');
% xlabel('Scores');


