

function [Score, ScoreCorrected,pkcorr] = ScoreV2_3Corrected(pkorig,pkcorr, dist,TripMulti)

    sumofPeak = sum(pkcorr)*TripMulti;
    Numerator = log(sumofPeak+1)/log(1.5);
    Denominator = log((dist*TripMulti)+1.01);
    Badness = (Numerator/Denominator)^0.5;
    ScoreCorrected = 110-26*Badness;
    
    sumofPeak = sum(pkorig)*TripMulti;
    Numerator = log(sumofPeak+1)/log(1.5);
    Denominator = log( (dist*TripMulti)+1.01);
    Badness = (Numerator/Denominator)^0.5;
    Score = 110-26*Badness;
    
    if Score > 100
       Score = 100;
    elseif Score < 0
        Score = 0;
    end
    
    if ScoreCorrected > 100
       ScoreCorrected = 100;
    elseif ScoreCorrected < 0
        ScoreCorrected = 0;
    end
end