
% Fetch Events for a given TripID

% sql = ['SELECT * FROM Event ' ...
%        'WHERE  tripid = ''c732575d-6662-5ff4-70b7-85d531987bc8''' ];

% TripID = '69783cfe-2d7d-884c-a555-e8c1e558a285';
sql = ['SELECT * from Event WHERE tripid = ''', TripID, ''''];
cursEvt = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');

cursEvt = fetch(cursEvt);
Events = (cursEvt.Data);  