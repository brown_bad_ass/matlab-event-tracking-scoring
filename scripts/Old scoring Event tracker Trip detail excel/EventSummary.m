



[r,c] = size(fileCell);
AccEventSummArr = zeros(6,14);
BrakeEventSummArr = zeros(6,11);
SpeedEventSummArr = zeros(6,11);

AccelEventCell = cell(6,1);
BrakeEventCell = cell(6,1);
SpeedEventCell = cell(6,1);

AccelScoreArr = zeros(6,1);
BrakeScoreArr = zeros(6,1);

PitchAvgArr = zeros(6,1);

% Penalty1525 = 2;
% Penalty2535 = 5;
% Penalty35 = 7;

AccPenalty = [3 5 10];
BrkPenalty = [1 5 7];

AvgSpeedScaling = 1;



for filenum=1:6
    
file = xlsread(fileCell{filenum,1});

RoadPitchPureAccelDistance
EventTracker

[NumAccelEvent,col] = size(AccelEvents);
[NumBrakeEvent,col] = size(BrakeEvents);
[NumSpeedEvent,col] = size(SpeedEvents);

AccelEventCell{filenum,1} = AccelEvents;
BrakeEventCell{filenum,1} = BrakeEvents;
SpeedEventCell{filenum,1} = SpeedEvents;
AccEventSumm =  [Distance/1000 TripDuration LitPer100 NumAccelEvent sum(AccelEvents(:,2)) sum(AccelEvents(:,2))/(Distance/1000) sum(AccelEvents(:,3)) sum(AccelEvents(:,3))/(Distance/1000) sum(AccelEvents(:,4)) sum(AccelEvents(:,4))/(Distance/1000) RPMAvg mean(AccelEvents(:,6)) SpeedAvg SpeedStd];
AccEventSummArr(filenum,:) = AccEventSumm;
BrakeEventSumm = [Distance/1000 TripDuration LitPer100 NumBrakeEvent sum(BrakeEvents(:,2)) sum(BrakeEvents(:,2))/(Distance/1000) sum(BrakeEvents(:,3)) sum(BrakeEvents(:,3))/(Distance/1000) sum(BrakeEvents(:,4)) sum(BrakeEvents(:,4))/(Distance/1000) SpeedAvg];
BrakeEventSummArr(filenum,:) = BrakeEventSumm;
SpeedEventSumm = [Distance/1000 TripDuration LitPer100 NumSpeedEvent sum(SpeedEvents(:,2)) sum(SpeedEvents(:,2))/(Distance/1000) sum(SpeedEvents(:,3)) sum(SpeedEvents(:,3))/(Distance/1000) sum(SpeedEvents(:,4)) sum(SpeedEvents(:,4))/(Distance/1000) SpeedAvg];
SpeedEventSummArr(filenum,:) = SpeedEventSumm;

% if AccEventSumm(13) < 20
%     AvgSpeedScaling = 0.8;
% end
% 
% if AccEventSumm(13) > 40
%     AvgSpeedScaling = 2.4;
% end

AccelScore = 100-(AvgSpeedScaling*(AccPenalty(1)*AccEventSumm(6)+AccPenalty(2)*AccEventSumm(8)+AccPenalty(3)*AccEventSumm(10)));
BrakeScore = 100-(AvgSpeedScaling*(BrkPenalty(1)*BrakeEventSumm(6)+BrkPenalty(2)*BrakeEventSumm(8)+BrkPenalty(3)*BrakeEventSumm(10)));

if AccelScore < 0
    AccelScore = 0;
end

AccelScoreArr(filenum) = AccelScore;
BrakeScoreArr(filenum) = BrakeScore;

end

