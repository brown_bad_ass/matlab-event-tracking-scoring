

% This script analyses trip summaries

StartWeek = 39;
EndWeek = 53;

TripDistanceCol = 1;
TripLitreCol    = 2;
TripKmPer100Col = 19;

data = DomTripSummary;
[row,col] = size(data);

wkTotalDistance = sum(data(StartWeek:EndWeek,TripDistanceCol));
wkTotalLitre    = sum(data(StartWeek:EndWeek,TripLitreCol));
wkAvgEconomy    = mean(data(StartWeek:EndWeek,TripKmPer100Col));

figure 
bar(data(StartWeek:EndWeek,TripDistanceCol));
title('Weekly distance');
ylabel('Distance in Km');
xlabel('Trip Number');

figure 
bar(data(StartWeek:EndWeek,TripLitreCol));
title('Weekly fuel consumption');
ylabel('Litre');
xlabel('Trip Number');

figure 
bar(data(StartWeek:EndWeek,TripKmPer100Col));
title('Weekly fuel economy');
ylabel('LitrePer100K');
xlabel('Trip Number');


