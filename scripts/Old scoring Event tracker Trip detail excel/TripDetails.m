

% This scrip generates histogram for various driving parameters

CarSizePenalty = 0.0;

SpeedCol = 4;
ThrottleCol = 6;
RpmCol = 5;
AccelCol = 10;
YAxisAccelCol = 11;
DistanceCol = 7;

MaxAccelStd = 0.093;
MaxBrakeStd = 0.086;

MinAccelStd  =  0.041;
MinBrakeStd  =  0.034;
MinCornerStd =  0.051;

HarshAccelCount = 0;
HarshBrakeCount = 0;
StopCount = 0;

load('Dom_Sep28_2015_5_02.mat');
data = Dom_Sep28_2015_5_02;
[row,col] = size(data);

TripDistance  = data(row,DistanceCol);

BaselineTP = FindBaseLineThrottle(data,ThrottleCol,SpeedCol);

data(:,ThrottleCol) = data(:,ThrottleCol)-floor(BaselineTP);

figure 
Speedhistogram = histogram(data(:,SpeedCol),20);

title('Speed histogram');
ylabel('Occurences');
xlabel('Speed in Km/hr');
figure 
Thottlehistogram = histogram(data(:,ThrottleCol),20);
title('Throttle histogram');
ylabel('Occurences');
xlabel('Throttle Press');

SpeedUnder20 = sum(Speedhistogram.Values(1:5));

TripDurationUnder20KperHr = (SpeedUnder20/row)*100;

pieData = [SpeedUnder20,row-SpeedUnder20];

labels = {'SpeedUnder20K','SpeedOver20K'};
figure
pie(pieData,labels);

TPUnder5 = sum(Thottlehistogram.Values(1:2));

TripDurationTPUnder5 = (TPUnder5/row)*100;

pieData = [TPUnder5,row-TPUnder5];

labels = {'TPUnder5','TPOver5'};
figure
pie(pieData,labels);

PositiveAccel = zeros(row,1);
NegetiveAccel = zeros(row,1);

for i=1:row
    
    if data(i,AccelCol) > 0
        
        PositiveAccel(i) = data(i,AccelCol);
        
    else
        
        NegetiveAccel(i) = data(i,AccelCol);
    end
    
end

AccStd = std(PositiveAccel);
BrkStd = std(NegetiveAccel);
CornerStd = std(data(:,YAxisAccelCol));

AccScore = (1/AccStd)/(1/MinAccelStd)*100*(1-CarSizePenalty);
BrkScore = (1/BrkStd)/(1/MinBrakeStd)*100*(1-CarSizePenalty);
CornerScore = (1/CornerStd)/(1/MinCornerStd)*100*(1-CarSizePenalty);


for i=1:row
    
    if data(i,AccelCol) > 0.25
        
        HarshAccelCount = HarshAccelCount+1;
        
    end
    
    if data(i,AccelCol) < -0.25

        HarshBrakeCount = HarshBrakeCount+1;
        
   end

end

for i=1:row
    
    if data(i,SpeedCol) == 0
        
        StopCount = StopCount+1;
        
    end

end

pieData = [StopCount,row-StopCount];

labels = {'StopTime','DrivingTime'};
figure
pie(pieData,labels);

HarshAccelPer100K = (HarshAccelCount/TripDistance)*100;
HarshBrakePer100K = (HarshBrakeCount/TripDistance)*100;

Scores = [AccScore,BrkScore,CornerScore];











