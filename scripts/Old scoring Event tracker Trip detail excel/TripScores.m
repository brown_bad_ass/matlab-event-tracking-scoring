


CarSizePenalty = 0.0;

SpeedCol = 4;
ThrottleCol = 6;
RpmCol = 5;
AccelCol = 10;
YAxisAccelCol = 11;
DistanceCol = 7;

MaxAccelStd = 0.093;
MaxBrakeStd = 0.086;

MinAccelStd  =  0.041;
MinBrakeStd  =  0.034;
MinCornerStd =  0.051;

HarshAccelCount = 0;
HarshBrakeCount = 0;

load('Dom_Sep28_2015_5_02.mat');
data = Dom_Sep28_2015_5_02;
[row,col] = size(data);

TripDistance  = data(row,DistanceCol);

BaselineTP = FindBaseLineThrottle(data,ThrottleCol,SpeedCol);

PositiveAccel = zeros(row,1);
NegetiveAccel = zeros(row,1);

for i=1:row
    
    if data(i,AccelCol) > 0
        
        PositiveAccel(i) = data(i,AccelCol);
        
    else
        
        NegetiveAccel(i) = data(i,AccelCol);
    end
    
end

AccStd = std(PositiveAccel);
BrkStd = std(NegetiveAccel);
CornerStd = std(data(:,YAxisAccelCol));

AccScore = (1/AccStd)/(1/MinAccelStd)*100*(1-CarSizePenalty);
BrkScore = (1/BrkStd)/(1/MinBrakeStd)*100*(1-CarSizePenalty);
CornerScore = (1/CornerStd)/(1/MinCornerStd)*100*(1-CarSizePenalty);


for i=1:row
    
    if data(i,AccelCol) > 0.25
        
        HarshAccelCount = HarshAccelCount+1;
        
    end
    
    if data(i,AccelCol) < -0.25

        HarshBrakeCount = HarshBrakeCount+1;

   end

end


HarshAccelPer100K = (HarshAccelCount/TripDistance)*100;
HarshBrakePer100K = (HarshBrakeCount/TripDistance)*100;

Scores = [AccScore,BrkScore,CornerScore];

disp('Accel Score')
disp(Scores(1))
disp('Braking Score')
disp(Scores(2))
disp('Cornering Score')
disp(Scores(3))

disp('Harsh Acceleration Per 100Km')
disp(HarshAccelPer100K)
disp('Harsh Braking Per 100Km')
disp(HarshBrakePer100K)

figure 
hist(data(:,SpeedCol));
title('Speed histogram');
ylabel('Occurences');
xlabel('Speed in Km/hr');
print('-f1','-djpeg','results/Speed Histogram.jpg');
figure 
hist(data(:,ThrottleCol));
title('Throttle Histogram');
ylabel('Occurences');
xlabel('Throttle Level');
print('-f2','-djpeg','results/THrottle Histogram.jpg');

