



%Tracking Acceleration Event

AccelThresh = 0.15;
AccelStart = 0;

BrakeThresh = -0.15;
BrakeStart = 0;

SpeedThresh = 80;
SpeedStart = 0;

AccelDuration12 = 0;
AccelDuration23 = 0;
AccelDuration3 = 0;


BrakeDuration12 = 0;
BrakeDuration23 = 0;
BrakeDuration3 = 0;

SpeedDuration1011 = 0;
SpeedDuration1112 = 0;
SpeedDuration12 = 0;

AccelPeak = 0;
BrakePeak = 0;
SpeedPeak = 0;


AccPenalty = [3 5 10];
BrkPenalty = [2 5 10];
SpdPenalty = [0.25 0.5 1];

AvgSpeedScaling = 1.5;

SchmittMargin = 0.03;
SchmittMarginSpeed = 5;


data = TripData;

[r,c] = size(data);

AccelEvents = [];
BrakeEvents = [];
SpeedEvents = [];
StopEvents = [];

StopsCount = 0;
StopDuration = 0;
StopTriggered = 0;

BrakeEventSumm = zeros(1,6);
AccEventSumm = zeros(1,6);
SpeedEventSumm = zeros(1,6);

Distance = data(r,4);
AvgSpeed = mean(data(:,6));

for i=1:r
  
    if StopTriggered == 0
        
        if data(i,6) == 0
            StopTriggered = 1;
        end
        
    end
    
    if StopTriggered == 1
        
        if data(i,6) == 0
            
            StopDuration = StopDuration + data(i,3); 
            
        else
            
            % Stop event finished, vehicle in motion again
            StopTriggered =  0;
            StopEvents = vertcat(StopEvents,StopDuration);
            StopDuration = 0;
            StopsCount = StopsCount+1;
            
        end
        
    end
    
    
    if AccelStart == 0
        
        if data(i,2) > AccelThresh

            AccelStart = 1;        
            
        end
    
    end
    
    if AccelStart == 1     
        
        
        if data(i,2) > AccelPeak
            
            AccelPeak = data(i,2);
            
        end
               
        
        if  ((data(i,2) > AccelThresh) && (data(i,2) <= (AccelThresh+0.10)))
            
            AccelDuration12 = AccelDuration12+data(i,3);
            
        end
        
        if ((data(i,2) > (AccelThresh+0.10)) && (data(i,2) <= (AccelThresh+0.20)))
            
            AccelDuration23 = AccelDuration23+data(i,3);
            
        end
        
        if data(i,2) > (AccelThresh+0.20)
        
            AccelDuration3 = AccelDuration3+data(i,3);
        
        end
        
        
        if data(i,2) < (AccelThresh-SchmittMargin)    
            
            AccelStart = 0;
           
            AccEvtSeverity = AccelDuration12*AccPenalty(1) + AccelDuration23*AccPenalty(2) + AccelDuration3*AccPenalty(3);
            temp = [AccelPeak AccelDuration12 AccelDuration23 AccelDuration3 AccEvtSeverity];
            AccelEvents = vertcat(AccelEvents,temp);       
                  
            AccelPeak       = 0; % Reset accel Peak
            AccelDuration12 = 0; % Reset duration
            AccelDuration23 = 0; % Reset duration
            AccelDuration3 = 0;  % Reset duration
            
            
        end
        
        
    end
    
    if BrakeStart == 0
        
        if data(i,2) < BrakeThresh

            BrakeStart = 1;        
            
        end
        
    end
    
    if BrakeStart == 1     
        
        
        if data(i,2) < BrakePeak
            
            BrakePeak = data(i,2);
            
        end
  
        
        if  ((data(i,2) < BrakeThresh) && (data(i,2) >= (BrakeThresh-0.1) ))
            
            BrakeDuration12 = BrakeDuration12+data(i,3);
            
        end
        
        if ((data(i,2) < (BrakeThresh-0.1)) && (data(i,2) >= (BrakeThresh-0.2)))
            
            BrakeDuration23 = BrakeDuration23+data(i,3);
            
        end
        
        if data(i,2) < (BrakeThresh-0.2)
        
            BrakeDuration3 = BrakeDuration3+data(i,3);
        
        end
        
        
        if data(i,2) > (BrakeThresh+SchmittMargin)    
            
            BrakeStart = 0;
            
            BrkEvtSeverity = BrakeDuration12*BrkPenalty(1) + BrakeDuration23*BrkPenalty(2) + BrakeDuration3*BrkPenalty(3);
            
            temp2 = [BrakePeak BrakeDuration12 BrakeDuration23 BrakeDuration3 BrkEvtSeverity];
            BrakeEvents = vertcat(BrakeEvents,temp2);       
                  
            BrakePeak       = 0; % Reset accel Peak
            BrakeDuration12 = 0; % Reset duration
            BrakeDuration23 = 0; % Reset duration
            BrakeDuration3 = 0;  % Reset duration 
            
        end
        
        
    end
    
    if SpeedStart == 0
        
        if data(i,6) > SpeedThresh
            
            SpeedStart = 1;
            
        end
    end
    
    if SpeedStart == 1
        
           
        if data(i,6) > SpeedPeak
            
            SpeedPeak = data(i,6);
            
        end
  
        
        if  ((data(i,6) > SpeedThresh) && (data(i,6) <= (SpeedThresh+10) ))
            
            SpeedDuration1011 = SpeedDuration1011+data(i,3);
            
        end
        
        if ((data(i,6) > SpeedThresh+10) && (data(i,6) <= (SpeedThresh+20) ))
            
            SpeedDuration1112 = SpeedDuration1112+data(i,3);
            
        end
        
        if data(i,6) > SpeedThresh+20
        
            SpeedDuration12 = SpeedDuration12+data(i,3);
        
        end
        
        
        if data(i,6) < (SpeedThresh-SchmittMarginSpeed)    
            
            SpeedStart = 0;
            
            temp3 = [SpeedPeak SpeedDuration1011 SpeedDuration1112 SpeedDuration12];
            SpeedEvents = vertcat(SpeedEvents,temp3);       
                  
            SpeedPeak       = 0; % Reset speeed Peak
            SpeedDuration1011 = 0; % Reset duration
            SpeedDuration1112 = 0; % Reset duration
            SpeedDuration12 = 0;  % Reset duration
                
            
        end
        
    end
    
    
    
end

if isempty(AccelEvents) == 0
    AccEventSumm = [sum(AccelEvents(:,2)) sum(AccelEvents(:,2))/(Distance) sum(AccelEvents(:,3)) sum(AccelEvents(:,3))/(Distance) sum(AccelEvents(:,4)) sum(AccelEvents(:,4))/(Distance)];
end

if isempty(BrakeEvents) == 0
    BrakeEventSumm = [sum(BrakeEvents(:,2)) sum(BrakeEvents(:,2))/(Distance) sum(BrakeEvents(:,3)) sum(BrakeEvents(:,3))/(Distance) sum(BrakeEvents(:,4)) sum(BrakeEvents(:,4))/(Distance)];
end

if isempty(SpeedEvents) == 0
    SpeedEventSumm = [sum(SpeedEvents(:,2)) sum(SpeedEvents(:,2))/(Distance) sum(SpeedEvents(:,3)) sum(SpeedEvents(:,3))/(Distance) sum(SpeedEvents(:,4)) sum(SpeedEvents(:,4))/(Distance)];
end


if AvgSpeed < 20
    AvgSpeedScaling = 0.8;
end

if (AvgSpeed > 40) && (AvgSpeed <= 60)
    AvgSpeedScaling = 2.4;
end

if AvgSpeed > 60
    AvgSpeedScaling = 3.2;
end

AccelScore = 100-(AvgSpeedScaling*(AccPenalty(1)*AccEventSumm(2)+AccPenalty(2)*AccEventSumm(4)+AccPenalty(3)*AccEventSumm(6)));
BrakeScore = 100-(AvgSpeedScaling*(BrkPenalty(1)*BrakeEventSumm(2)+BrkPenalty(2)*BrakeEventSumm(4)+BrkPenalty(3)*BrakeEventSumm(6)));
SpeedScore = 100-(AvgSpeedScaling*(SpdPenalty(1)*SpeedEventSumm(2)+SpdPenalty(2)*SpeedEventSumm(4)+SpdPenalty(3)*SpeedEventSumm(6)));

if AccelScore < 0
    
    AccelScore = 0;
    
end

if BrakeScore < 0
    
    BrakeScore = 0;
    
end

if SpeedScore < 0
    
    SpeedScore = 0;
    
end

DriverScore = 0.3*AccelScore + 0.3*BrakeScore + 0.4*SpeedScore ;
IdlingPerKm = StopsCount/Distance;
