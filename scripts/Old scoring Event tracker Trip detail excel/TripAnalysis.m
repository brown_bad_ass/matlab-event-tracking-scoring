

CarSizePenalty = 0.0;

SpeedCol = 3;
ThrottleCol = 5;
RpmCol = 4;
AccelCol = 9;
YAxisAccelCol = 10;
DistanceCol = 6;

MaxAccelStd = 0.093;
MaxBrakeStd = 0.086;

MinAccelStd  =  0.041;
MinBrakeStd  =  0.034;
MinCornerStd =  0.051;

HarshAccelCount = 0;
HarshBrakeCount = 0;
StopCount = 0;

SpeedVec = TripDetail(:,SpeedCol);
SpeedVec = cell2mat(SpeedVec);

ThrottleVec = TripDetail(:,ThrottleCol);
ThrottleVec = cell2mat(ThrottleVec);

RPMVec = TripDetail(:,RpmCol);
RPMVec = cell2mat(RPMVec);

XAxisVec = TripDetail(:,AccelCol);
XAxisVec = cell2mat(XAxisVec);

MinTP = min(ThrottleVec);
ThrottleVec = ThrottleVec-MinTP;

figure 
Speedhistogram = histogram(SpeedVec,20);
title('Speed histogram');
ylabel('Occurences');
xlabel('Speed in Km/hr');

figure 
Thottlehistogram = histogram(ThrottleVec,20);
title('Throttle histogram');
ylabel('Occurences');
xlabel('Throttle Press');

