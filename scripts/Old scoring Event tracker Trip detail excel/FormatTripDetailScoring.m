




TripDeltaT = [];

TripDeltaT(1) = 2;
sortedData = sortrows(TripDetail,18);
[row,col] = size(sortedData);

TripData = zeros(row,7);

for TripDetailNum = 2:row
    temp = 86400*(datenum( sortedData{TripDetailNum,18}, 'yyyy-mm-dd HH:MM:SS' ) - datenum( sortedData{TripDetailNum-1,18}, 'yyyy-mm-dd HH:MM:SS' )) ;
    TripDeltaT = vertcat(TripDeltaT,temp);
end

TripData(:,2) = cell2mat(sortedData(:,9));
TripData(:,3) = TripDeltaT;
TripData(:,4) = cell2mat(sortedData(:,6));
TripData(:,6) = cell2mat(sortedData(:,3));
TripData(:,7) = cell2mat(sortedData(:,7));

TripData(1:5,:) = [];
