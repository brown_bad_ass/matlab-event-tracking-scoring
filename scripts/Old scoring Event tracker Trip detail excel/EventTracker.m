

%Tracking Acceleration Event

AccelThresh = 0.15;
AccelStart = 0;

BrakeThresh = -0.1;
BrakeStart = 0;

SpeedThresh = 20;
SpeedStart = 0;

AccelDuration12 = 0;
AccelDuration23 = 0;
AccelDuration3 = 0;


BrakeDuration12 = 0;
BrakeDuration23 = 0;
BrakeDuration3 = 0;

SpeedDuration1011 = 0;
SpeedDuration1112 = 0;
SpeedDuration12 = 0;

AccelPeak = 0;
BrakePeak = 0;
SpeedPeak = 0;

RPMPeak = 0;
RPMAvg = 0;
RPMArr = [];
PitchArr = [];
PitchAvg = 0;


SchmittMargin = 0.03;
SchmittMarginSpeed = 1;

data = PitchAccelDurationDistanceRPMVss;

[r,c] = size(data);

AccelEvents = [];
BrakeEvents = [];
SpeedEvents = [];

for i=1:r
  
    if AccelStart == 0
        
        if data(i,2) > AccelThresh

            AccelStart = 1;        
            
        end
    
    end
    
    if AccelStart == 1     
        
        
        if data(i,2) > AccelPeak
            
            AccelPeak = data(i,2);
            
        end
        
        if data(i,5) > RPMPeak
            
            RPMPeak = data(i,5);
            
        end
        
        RPMArr = vertcat(RPMArr,data(i,5));
        PitchArr = vertcat(PitchArr,data(i,1));
        
        if  ((data(i,2) > AccelThresh) && (data(i,2) <= (AccelThresh+0.10)))
            
            AccelDuration12 = AccelDuration12+data(i,3);
            
        end
        
        if ((data(i,2) > (AccelThresh+0.10)) && (data(i,2) <= (AccelThresh+0.20)))
            
            AccelDuration23 = AccelDuration23+data(i,3);
            
        end
        
        if data(i,2) > (AccelThresh+0.20)
        
            AccelDuration3 = AccelDuration3+data(i,3);
        
        end
        
        
        if data(i,2) < (AccelThresh-SchmittMargin)    
            
            AccelStart = 0;
            RPMAvg = mean(RPMArr);
            PitchAvg = mean(PitchArr);
            temp = [AccelPeak AccelDuration12 AccelDuration23 AccelDuration3 RPMPeak RPMAvg PitchAvg];
            AccelEvents = vertcat(AccelEvents,temp);       
                  
            AccelPeak       = 0; % Reset accel Peak
            AccelDuration12 = 0; % Reset duration
            AccelDuration23 = 0; % Reset duration
            AccelDuration3 = 0;  % Reset duration
            
            RPMPeak = 0;
            RPMArr = [];
            PitchArr = [];
            
        end
        
        
    end
    
    if BrakeStart == 0
        
        if data(i,2) < BrakeThresh

            BrakeStart = 1;        
            
        end
        
    end
    
    if BrakeStart == 1     
        
        
        if data(i,2) < BrakePeak
            
            BrakePeak = data(i,2);
            
        end
  
        
        if  ((data(i,2) < BrakeThresh) && (data(i,2) >= (BrakeThresh-0.1) ))
            
            BrakeDuration12 = BrakeDuration12+data(i,3);
            
        end
        
        if ((data(i,2) < (BrakeThresh-0.1)) && (data(i,2) >= (BrakeThresh-0.2)))
            
            BrakeDuration23 = BrakeDuration23+data(i,3);
            
        end
        
        if data(i,2) < (BrakeThresh-0.2)
        
            BrakeDuration3 = BrakeDuration3+data(i,3);
        
        end
        
        
        if data(i,2) > (BrakeThresh+SchmittMargin)    
            
            BrakeStart = 0;
            
            temp2 = [BrakePeak BrakeDuration12 BrakeDuration23 BrakeDuration3];
            BrakeEvents = vertcat(BrakeEvents,temp2);       
                  
            BrakePeak       = 0; % Reset accel Peak
            BrakeDuration12 = 0; % Reset duration
            BrakeDuration23 = 0; % Reset duration
            BrakeDuration3 = 0;  % Reset duration
            
       
            
        end
        
        
    end
    
    if SpeedStart == 0
        
        if data(i,6) > SpeedThresh
            
            SpeedStart = 1;
            
        end
    end
    
    if SpeedStart == 1
        
           
        if data(i,6) > SpeedPeak
            
            SpeedPeak = data(i,6);
            
        end
  
        
        if  ((data(i,6) > SpeedThresh) && (data(i,6) <= (SpeedThresh+10) ))
            
            SpeedDuration1011 = SpeedDuration1011+data(i,3);
            
        end
        
        if ((data(i,6) > SpeedThresh+10) && (data(i,6) <= (SpeedThresh+20) ))
            
            SpeedDuration1112 = SpeedDuration1112+data(i,3);
            
        end
        
        if data(i,6) > SpeedThresh+20
        
            SpeedDuration12 = SpeedDuration12+data(i,3);
        
        end
        
        
        if data(i,6) < (SpeedThresh-SchmittMarginSpeed)    
            
            SpeedStart = 0;
            
            temp3 = [SpeedPeak SpeedDuration1011 SpeedDuration1112 SpeedDuration12];
            SpeedEvents = vertcat(SpeedEvents,temp3);       
                  
            SpeedPeak       = 0; % Reset speeed Peak
            SpeedDuration1011 = 0; % Reset duration
            SpeedDuration1112 = 0; % Reset duration
            SpeedDuration12 = 0;  % Reset duration
            
       
            
        end
        
    end
    
    
    
end
