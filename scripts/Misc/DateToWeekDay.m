

weekdays = cell({});
TripIdDis10 = cell({});

[row,col] = size(DataMat);

for i=2:row-1
    
    if DataMat{i,1} > 10
           TripId = DataMat{i,15};
           TripIdDis10 = vertcat(TripIdDis10,(TripId)); 
    end
    
    date       = DataMat{i,9};
    [num,name] = weekday(date);
    weekdays   = vertcat(weekdays,name);
end