

% Computing pitch of Road, pure linear accel and Distance

M_PI = 3.14159;
GRAVITY =  9.81;
weight = 0.995;

Km_To_M_Per_Sec = 5/18;
Distance = 0;
TripDuration = 0;


% file = xlsread('GoFar Test Track Agressive.xlsx');

[m,n] = size(file);

PitchAccelDurationDistanceRPMVss = zeros(m,6);
complementary_car_pitch_Vss_corrected = 0;

% Column headers

XSensorCol = 5; % Raw accel col 
YSensorCol = 6;
ZSensorCol = 7;

XCarCol = 8; % Car frame accel
YCarCol = 9;
ZCarCol = 10;

XSensorColGyro = 11; % Raw Gyro Col
YSensorColGyro = 12;
ZSensorColGyro = 13;

XCarColGyro = 14; % Car frame Gyro
YCarColGyro = 15;
ZCarColGyro = 16;

RPMCol = 20;
SpeedDerivedAccCol = 17; 
SpeedCol = 18; % LOw pass filtered floating point
GyroDtCol = 4;
LitreCol = 21;

%Complementary filter 

for i=1:m
    
    
X_Axis_Accel_Car_Frame = file(i,XCarCol);
Vss_Derived_Accel = file(i,SpeedDerivedAccCol);

Y_Axis_Accel_Car_Frame = file(i,YCarCol);
Z_Axis_Accel_Car_Frame = file(i,ZCarCol);

Y_Gyro_Car_Frame =  file(i,YCarColGyro);
dT = file(i,GyroDtCol)/1000;


TimeBetweenDebugPrint = file(i,3);

RPM = file(i,RPMCol);
Vss = file(i,SpeedCol);

dummy = zeros(19,2);

    for j=1:19  
    %Calculate pitch from X axis car cordinates (corrected with Vss derived
    %accel 
    
    car_pitch_deg_Vss_corrected = (atan2((X_Axis_Accel_Car_Frame-Vss_Derived_Accel),sqrt(Y_Axis_Accel_Car_Frame*Y_Axis_Accel_Car_Frame+Z_Axis_Accel_Car_Frame*Z_Axis_Accel_Car_Frame))*180)/M_PI;
    
    %Calculate pitch angle through complementary filter feeding Y Axis Gyro
    %car cordinates and car_pitch_deg_Vss_corrected
    

%     car_pitch_deg_Vss_corrected = car_pitch_deg_Vss_corrected-PitchBias;
    complementary_car_pitch_Vss_corrected = weight*(complementary_car_pitch_Vss_corrected+ (Y_Gyro_Car_Frame*dT))+((1-weight)*car_pitch_deg_Vss_corrected);
    Car_Pure_Linear_Accel = X_Axis_Accel_Car_Frame - sin(complementary_car_pitch_Vss_corrected*M_PI/180);
    
    dummy(j,1) = Car_Pure_Linear_Accel;
    dummy(j,2) = complementary_car_pitch_Vss_corrected;
     
    end
    Distance = Distance + (file(i,SpeedCol)*TimeBetweenDebugPrint*Km_To_M_Per_Sec);
    TripDuration = TripDuration + TimeBetweenDebugPrint;
    PitchAccelDurationDistanceRPMVss(i,1) = mean(dummy(:,2));
    PitchAccelDurationDistanceRPMVss(i,2) = mean(dummy(:,1));
    PitchAccelDurationDistanceRPMVss(i,3) = TimeBetweenDebugPrint;
    PitchAccelDurationDistanceRPMVss(i,4) = Distance;
    PitchAccelDurationDistanceRPMVss(i,5) = RPM;
    PitchAccelDurationDistanceRPMVss(i,6) = Vss;
end

LitPer100 = (file(m,LitreCol)/10000000)/(Distance/1000)*100;
SpeedStd = std(file(:,SpeedCol));
SpeedAvg = mean(file(:,SpeedCol));
RPMAvg = mean(file(:,RPMCol));
PitchAvg = mean(PitchAccelDurationDistanceRPMVss(:,1));

% figure
% plot(PitchAccelDurationDistanceRPMVss(:,2));



