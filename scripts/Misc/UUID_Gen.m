
%Experimenting with random numbers and UUID

function [out] = UUID_Gen(seed)

    m = 2^31;
    c = 12345;
    a = 1103515245; 
    
    out = mod((a*seed + c),m);

end