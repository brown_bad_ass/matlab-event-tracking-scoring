

function [BaseTP] =  FindBaseLineThrottle(data,ThrottleCol,SpeedCol)

[row,col] = size(data);

TP = [];
for i=1:row
    
    if data(i,SpeedCol) == 0
        TP = vertcat(data(i,ThrottleCol),TP);
    end

end

BaseTP = mean(TP);


end