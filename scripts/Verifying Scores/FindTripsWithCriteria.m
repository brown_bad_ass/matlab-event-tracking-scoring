

% Find trips with specific criteria for a given vehicle

VehicleID = '8ca0f8d2-f0e3-4911-82ff-2b34ea22ab45';
sql = ['SELECT * from trip WHERE vehicleid = ''', VehicleID, ''''];


curstrip = exec(conn,sql);
setdbprefs('DataReturnFormat','cellarray');

curstrip = fetch(curstrip);
tripsSumm = (curstrip.Data);  

%Iterate through all the trips and find the ids that satisfy distance and 
%calibration not running criteria

[NumOfSumm,col]= size(tripsSumm);

TripIDCriteria = cell(NumOfSumm,1);

for i=1:NumOfSumm 
   
    Distance = tripsSumm{i,1};
    CalRunning = tripsSumm{i,30};
    
    if ( (Distance > 10) && (Distance < 15) && (CalRunning == 0) )
        
       TripIDCriteria{i,1} = char(tripsSumm{i,11}); 
        
    end
    
end


