%  Score V2 for all the trips
DurationCnt =  1;
DurationSumCnt =  1;
DurationArr = zeros(1,3);
DurationSumArr = zeros(1,6);

[NumTrips,col] = size(tripsSumm);

ScoresV2 = cell(NumTrips,5);

for i=1:NumTrips
    
  TripID = char(tripsSumm{i,1}); %Convert java util UUID into char
  Distance = tripsSumm{i,2}; %Distance Col
  AvgSpeed = tripsSumm{i,3};%Avg Speed col check again 
  GenerateScoresEventsV2;
   
%    if (Distance < 10)
%        tmp = [Distance,AvgSpeed,AccScoreEvtV2,BrkScoreEvtV2];
%        ScoreV2Bucket1 = vertcat(ScoreV2Bucket1,tmp);
%    else
%        tmp = [Distance,AvgSpeed,AccScoreEvtV2,BrkScoreEvtV2];
%        ScoreV2Bucket2 = vertcat(ScoreV2Bucket2,tmp);
%    end

    ScoresV2{i,1} = Distance;
    ScoresV2{i,2} = AvgSpeed;
    ScoresV2{i,3} = AccScoreEvtV2;
    ScoresV2{i,4} = BrkScoreEvtV2;
    ScoresV2{i,5} = TripID;


   
end