
FetchTripSumm

% This script fetches trips of certain specified length and then fetches
% accel and braking scores and plots histogram

[SummRow,SummCol] = size(tripsSumm);

AccelScoreBin1  = [];
AccelScoreBin2  = [];

BrakeScoreBin1  = [];
BrakeScoreBin2  = [];

AccelScoreBin3  = zeros(SummRow,1);

AccelScoreCol = 25;
BrakeScoreCol = 22;
DistanceCol = 1;
AvgSpeedCol = 19;

DistanceThresh1 = 10;
DistanceThresh2 = 20;

DistanceVec = tripsSumm(:,DistanceCol);
DistanceVec = cell2mat(DistanceVec);

AccScoreVec = tripsSumm(:,AccelScoreCol);
AccScoreVec = cell2mat(AccScoreVec);

BrkScreVec = tripsSumm(:,BrakeScoreCol);
BrkScreVec = cell2mat(BrkScreVec);

AvgSpeedVec = tripsSumm(:,AvgSpeedCol);
AvgSpeedVec = cell2mat(AvgSpeedVec);

for i = 1:SummRow
   
    if (DistanceVec(i) <= DistanceThresh1 )
        tp1 = [DistanceVec(i),AvgSpeedVec(i),AccScoreVec(i)];
        tp2 = [DistanceVec(i),AvgSpeedVec(i),BrkScreVec(i)];
        
         AccelScoreBin1 = vertcat(AccelScoreBin1,tp1);
         
         BrakeScoreBin1 = vertcat(BrakeScoreBin1,tp2);
              
    else     
        
        tp1 = [DistanceVec(i),AvgSpeedVec(i),AccScoreVec(i)];
        tp2 = [DistanceVec(i),AvgSpeedVec(i),BrkScreVec(i)];
        
         AccelScoreBin2 = vertcat(AccelScoreBin2,tp1);
         BrakeScoreBin2 = vertcat(BrakeScoreBin2,tp2);
    end
    
    
end



figure 
hist(AccelScoreBin1(:,3));
title('Accel Score Hist , distance<=10km, Model v1');
ylabel('Occurences');
xlabel('Scores');

figure 
hist(AccelScoreBin2(:,3) );
title('Accel Score Hist , distance > 10km, Model v1');
ylabel('Occurences');
xlabel('Scores');

figure 
hist(BrakeScoreBin1(:,3) );
title('Brake Score Hist , distance<=10km, Model v1');
ylabel('Occurences');
xlabel('Scores');

figure 
hist(BrakeScoreBin2(:,3));
title('Brake Score Hist , distance > 10km, Model v1');
ylabel('Occurences');
xlabel('Scores');

figure 
scatter(AccelScoreBin1(:,1),AccelScoreBin1(:,3));
title('Scatter plot Accel Score vs Distance in Km, Model v1');
ylabel('Scores');
xlabel('Distance');

figure 
scatter(AccelScoreBin2(:,1),AccelScoreBin2(:,3));
title('Scatter plot Accel Score vs Distance in Km, Model v1');
ylabel('Scores');
xlabel('Distance');

figure 
scatter(AccelScoreBin1(:,2),AccelScoreBin1(:,3));
title('Scatter plot Accel Score vs AvgSpeed in Km/hr, Model v1');
ylabel('Scores');
xlabel('AvgSpeed');

figure 
scatter(AccelScoreBin2(:,2),AccelScoreBin2(:,3));
title('Scatter plot Accel Score vs AvgSpeed in Km/hr, Model v1');
ylabel('Scores');
xlabel('AvgSpeed');
