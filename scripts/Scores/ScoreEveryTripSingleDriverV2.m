% Fetch trip summaries to get all the trip IDs
FetchTripSumm % put the vehicle id 

%Score every trip from Single driver

[NumTrips,col] = size(tripsSumm);
ScoreV2Bucket1 = [];
ScoreV2Bucket2 = [];
AccScoreDist = [];

for i=1:NumTrips
    
  TripID = char(tripsSumm{i,11}); %Convert java util UUID into char
  Distance = tripsSumm{i,1}; %Distance Col
  AvgSpeed = tripsSumm{i,19};%Avg Speed col check again 
  GenerateScoresEventsV2;
   
   if (Distance < 10)
       tmp = [Distance,AvgSpeed,AccScoreEvtV2,BrkScoreEvtV2];
       ScoreV2Bucket1 = vertcat(ScoreV2Bucket1,tmp);
   else
       tmp = [Distance,AvgSpeed,AccScoreEvtV2,BrkScoreEvtV2];
       ScoreV2Bucket2 = vertcat(ScoreV2Bucket2,tmp);
   end
   
end

%------------Histograms-----------------------------------------
% figure 
% hist(ScoreV2Bucket1(:,3));
% title('Accel Score Hist model V2 , distance<=10km, Model v2');
% ylabel('Occurences');
% xlabel('Scores');
% 
% figure 
% hist(ScoreV2Bucket2(:,3));
% title('Accel Score Hist model V2 , distance > 10km, Model v2');
% ylabel('Occurences');
% xlabel('Scores');
% 
% figure 
% hist(ScoreV2Bucket1(:,4));
% title('Braking Score Hist model V2 , distance<=10km, Model v2');
% ylabel('Occurences');
% xlabel('Scores');
% 
% figure 
% hist(ScoreV2Bucket2(:,4));
% title('Braking Score Hist model V2 , distance > 10km, Model v2');
% ylabel('Occurences');
% xlabel('Scores');

%-----------End of -Histograms-----------------------------------------

%---------------Scatter--------------------------------------------
% figure 
% scatter(ScoreV2Bucket1(:,1),ScoreV2Bucket1(:,3));
% title('Scatter plot Accel Score vs Distance < 10 in Km, Model v2');
% ylabel('Scores');
% xlabel('Distance');
% 
% figure 
% scatter(ScoreV2Bucket2(:,1),ScoreV2Bucket2(:,3));
% title('Scatter plot Accel Score vs Distance > 10 in Km, Model v2');
% ylabel('Scores');
% xlabel('Distance');
% 
% figure 
% scatter(ScoreV2Bucket1(:,1),ScoreV2Bucket1(:,4));
% title('Scatter plot Braking Score vs Distance < 10 in Km, Model v2');
% ylabel('Scores');
% xlabel('Distance');
% 
% figure 
% scatter(ScoreV2Bucket2(:,1),ScoreV2Bucket2(:,4));
% title('Scatter plot Braking Score vs Distance > 10 in Km, Model v2');
% ylabel('Scores');
% xlabel('Distance');
% 
% figure 
% scatter(ScoreV2Bucket1(:,2),ScoreV2Bucket1(:,3));
% title('Scatter plot Accel Score vs AvgSpeed in Km/hr, Distance < 10 in Km Model v2');
% ylabel('Scores');
% xlabel('AvgSpeed');
% 
% figure 
% scatter(ScoreV2Bucket2(:,2),ScoreV2Bucket2(:,3));
% title('Scatter plot Accel Score vs AvgSpeed in Km/hr, Distance > 10 in KmModel v2');
% ylabel('Scores');
% xlabel('AvgSpeed');
% 
% figure 
% scatter(ScoreV2Bucket1(:,2),ScoreV2Bucket1(:,4));
% title('Scatter plot Braking Score vs AvgSpeed in Km/hr, Distance < 10 in Km Model v2');
% ylabel('Scores');
% xlabel('AvgSpeed');
% 
% figure 
% scatter(ScoreV2Bucket2(:,2),ScoreV2Bucket2(:,4));
% title('Scatter plot Braking Score vs AvgSpeed in Km/hr, Distance > 10 in KmModel v2');
% ylabel('Scores');
% xlabel('AvgSpeed');