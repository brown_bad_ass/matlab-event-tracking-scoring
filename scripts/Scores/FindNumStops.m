

%FInd number of stops from trip detail
NumStops = 0;
StopDetected = 0;

SpdCol = 3;
TimeCol = 16;

sortedTripDetail = sortrows(TripDetail,16);
[tRow,tCol] = size(sortedTripDetail);

for i = 1:tRow
    
    if ((sortedTripDetail{i,3} == 0) && (StopDetected == 0))
        
        StopDetected = 1;
        
    end
    
    if StopDetected == 1
        
        if sortedTripDetail{i,3} > 0
            StopDetected = 0;
            NumStops = NumStops+1;
        end
    end
    
end




