

%Generate scores based on actual events on server

% Fetch trip details and find the distance
% more efficient would be to fetch trip summary and get distance
FetchTripDetails

Distance = TripDetail(:,6); %Get distance column
Distance = cell2mat(Distance);% convert into a matrix
Distance = max(Distance); % find max which is the total length of trip

%Find average speed

AvgSpeed = TripDetail(:,3);
AvgSpeed = cell2mat(AvgSpeed);
AvgSpeed = mean(AvgSpeed);

%Fetch events from serve and parse it in a usable format
FetchEvents

ParseEvents

AccSumm = zeros(1,3);
BrkSumm = zeros(1,3);
SpdSumm = zeros(1,3);

% Score calculation starts here , original model 
% AccPenalty = [3 5 10];
% BrkPenalty = [2 5 10]; 
% SpdPenalty = [0.25 0.5 1];

AccPenalty = [5 7 10];
BrkPenalty = [3 6 10];
SpdPenalty = [0.25 0.5 1];


MilliSecToSec = 1000;

AccPenalty = AccPenalty/MilliSecToSec;
BrkPenalty = BrkPenalty/MilliSecToSec;
SpdPenalty = SpdPenalty/MilliSecToSec;


AvgSpeedScaling = 2;

if AvgSpeed < 30
    AvgSpeedScaling = 1;
end

if (AvgSpeed > 40) && (AvgSpeed <= 60)
    AvgSpeedScaling = 4;
end

if AvgSpeed > 60
    AvgSpeedScaling = 6;
end

if isempty(DurationBinAcc) == 0
    AccSumm = [sum(DurationBinAcc(:,1))/Distance sum(DurationBinAcc(:,2))/Distance sum(DurationBinAcc(:,3))/Distance];
end

if isempty(DurationBinBrk) == 0
    BrkSumm = [sum(DurationBinBrk(:,1))/Distance sum(DurationBinBrk(:,2))/Distance sum(DurationBinBrk(:,3))/Distance];
end

if isempty(DurationBinSpd) == 0
    SpdSumm = [sum(DurationBinSpd(:,1))/Distance sum(DurationBinSpd(:,2))/Distance sum(DurationBinSpd(:,3))/Distance];
end

AccelScoreEvt = 100-(AvgSpeedScaling*(AccPenalty(1)*AccSumm(1)+AccPenalty(2)*AccSumm(2)+AccPenalty(3)*AccSumm(3)));
BrakeScoreEvt = 100-(AvgSpeedScaling*(BrkPenalty(1)*BrkSumm(1)+BrkPenalty(2)*BrkSumm(2)+BrkPenalty(3)*BrkSumm(3)));
SpeedScoreEvt = 100-(AvgSpeedScaling*(SpdPenalty(1)*SpdSumm(1)+SpdPenalty(2)*SpdSumm(2)+SpdPenalty(3)*SpdSumm(3)));


if AccelScoreEvt < 0
    
    AccelScoreEvt = 0;
    
end

if BrakeScoreEvt < 0
    BrakeScoreEvt = 0;
    
end

if SpeedScoreEvt < 0
    SpeedScoreEvt = 0;
end

% DriverScoreEvt = AccelScoreEvt*0.3+BrakeScoreEvt*0.3+SpeedScoreEvt*0.4;
   
   
 DriverScoreEvt = AccelScoreEvt*0.5+BrakeScoreEvt*0.5;