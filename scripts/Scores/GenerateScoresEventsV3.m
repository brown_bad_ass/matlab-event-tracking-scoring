% Generate Scores from event duration and peaks Model V3

% Fetch trip details to find number of stops
% more efficient would be to fetch trip summary and get distance
FetchTripDetails

[TripR,TripC] = size(TripDetail);

if TripR > 1
%Fetch number of stops in that trip
    FindNumStops
    
%Fetch events from serve and parse it in a usable format
FetchEvents

[EvtR,EvtC] = size(Events);

%Proceed further if size is not NULL
if EvtR > 1
    
ParseEventsV2

AccScoreEvtV2 = 100;
BrkScoreEvtV2 = 100;

AccPenalty = [5 7 10];
BrkPenalty = [3 6 10];
SpdPenalty = [0.25 0.5 1];


MilliSecToSec = 1000;

AccPenalty = AccPenalty/MilliSecToSec;
BrkPenalty = BrkPenalty/MilliSecToSec;
SpdPenalty = SpdPenalty/MilliSecToSec;



% Deduct score per event 
if isempty(DurationBinAccPk) == 0
    DurationBinAccPk = cell2mat(DurationBinAccPk);
    DurationBinAccPkNorm = DurationBinAccPk(:,1:3)/NumStops;
%     DurationBinAccPkNorm = DurationBinAccPk(:,1:3)/Distance;
    [r,c] = size(DurationBinAccPk);
    for i=1:r
        AccScoreEvtV2 = AccScoreEvtV2-((AccPenalty(1)*DurationBinAccPkNorm(i,1)+AccPenalty(2)*DurationBinAccPkNorm(i,2)+AccPenalty(3)*DurationBinAccPkNorm(i,3))*(1+DurationBinAccPk(i,4))^3 );
    end
    if AccScoreEvtV2 < 0
        AccScoreEvtV2 = 0;
    end
end

end

end
