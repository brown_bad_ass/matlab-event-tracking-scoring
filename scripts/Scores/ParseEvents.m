

%Parse events cell into matrix of duration for different intensities for
%events

[EvtRow, EvtCol] = size(Events);

DurationBinAcc = [];
DurationBinBrk = [];
DurationBinSpd = [];


for ind = 1:EvtRow
    
    EventData = Events{ind,8};
    
    EventType = str2num(cell2mat(Events(ind,5)));
    
    IndexSemiColon = strfind(EventData,':');
    IndexComma = strfind(EventData,',');
    
    Bin1 = EventData(IndexSemiColon(1)+1:IndexComma(1)-1);
    Bin2 = EventData(IndexSemiColon(3)+1:IndexComma(3)-1);
    Bin3 = EventData(IndexSemiColon(5)+1:IndexComma(5)-1);
    
    
    
    if EventType == 1
        
        temp = [str2num(Bin1) str2num(Bin2) str2num(Bin3)];  
        DurationBinAcc =  vertcat(DurationBinAcc,temp);
     
    end
    
        
    if EventType == 2
        
        temp = [str2num(Bin1) str2num(Bin2) str2num(Bin3)];  
        DurationBinBrk =  vertcat(DurationBinBrk,temp);
           
    end
    
    if EventType == 3
        
        temp = [str2num(Bin1) str2num(Bin2) str2num(Bin3)];  
        DurationBinSpd =  vertcat(DurationBinSpd,temp);
        
    end

    
    
end

