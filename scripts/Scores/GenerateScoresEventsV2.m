%Generate Scores from event duration and peaks Model V2



%Fetch events from serve and parse it in a usable format
FetchEvents

[EvtR,EvtC] = size(Events);

if EvtR > 1
    
ParseEventsV2

AccScoreEvtV2 = 255;
BrkScoreEvtV2 = 255;

EngineSize = 1500;


AccPenalty = [5.5 8 10];
BrkPenalty = [5 8 12];
SpdPenalty = [0.25 0.5 1];


MilliSecToSec = 1000;

AccPenalty = AccPenalty/MilliSecToSec;
BrkPenalty = BrkPenalty/MilliSecToSec;
SpdPenalty = SpdPenalty/MilliSecToSec;


AvgSpeedScaling = 2;

if AvgSpeed < 30
    AvgSpeedScaling = 1;
end

if (AvgSpeed > 40) && (AvgSpeed <= 60)
    AvgSpeedScaling = 4;
end

if AvgSpeed > 60
    AvgSpeedScaling = 6;
end

% Deduct score per event 
if isempty(DurationBinAccPk) == 0
%     DurationBinAccPk = cell2mat(DurationBinAccPk);
    DurationBinAccPkNorm = DurationBinAccPk(:,1:3)/Distance;
    
    DurationSumArr(DurationSumCnt,1) = sum(DurationBinAccPk(:,1));
    DurationSumArr(DurationSumCnt,2) = sum(DurationBinAccPk(:,2));
    DurationSumArr(DurationSumCnt,3) = sum(DurationBinAccPk(:,3));
    
    DurationSumArr(DurationSumCnt,4) = sum(DurationBinAccPkNorm(:,1));
    DurationSumArr(DurationSumCnt,5) = sum(DurationBinAccPkNorm(:,2));
    DurationSumArr(DurationSumCnt,6) = sum(DurationBinAccPkNorm(:,3));
    
    DurationSumCnt = DurationSumCnt+1;
    
    [r,c] = size(DurationBinAccPk);
    for counter = 1:r
        AccDurationPart = AccPenalty(1)*DurationBinAccPkNorm(counter,1)+AccPenalty(2)*DurationBinAccPkNorm(counter,2)+AccPenalty(3)*DurationBinAccPkNorm(counter,3);
        AccScoreEvtV2 = AccScoreEvtV2-(AvgSpeedScaling*(AccDurationPart*((1.05+DurationBinAccPk(counter,4))^4)) );
        DurationArr(DurationCnt,1) = DurationBinAccPkNorm(counter,1);
        DurationArr(DurationCnt,2) = DurationBinAccPkNorm(counter,2);
        DurationArr(DurationCnt,3) = DurationBinAccPkNorm(counter,3);
        DurationCnt = DurationCnt+1;
        
    end
    if AccScoreEvtV2 < 0
        AccScoreEvtV2 = 0;
    end
end

if isempty(DurationBinBrkPk) == 0
%     DurationBinAccPk = cell2mat(DurationBinAccPk);
    DurationBinBrkPkNorm = DurationBinBrkPk(:,1:3)/Distance;
    [r,c] = size(DurationBinBrkPk);
    for counter = 1:r
        BrkScoreEvtV2 = BrkScoreEvtV2-(AvgSpeedScaling*(BrkPenalty(1)*DurationBinBrkPkNorm(counter,1)+BrkPenalty(2)*DurationBinBrkPkNorm(counter,2)+BrkPenalty(3)*DurationBinBrkPkNorm(counter,3))*(1+DurationBinBrkPk(counter,4))^4 );
    end
    if BrkScoreEvtV2 < 0
        BrkScoreEvtV2 = 0;
    end
end

AccScoreEvtV2 = AccScoreEvtV2/255*100; 
BrkScoreEvtV2 = BrkScoreEvtV2/255*100; 

end

% end
