
%Score every trip single driver V3

% Fetch trip summaries to get all the trip IDs
FetchTripSumm

[NumTrips,col] = size(tripsSumm);
AccelScoreV3Bin1 = [];
AccelScoreV3Bin2 = [];
for i=1:NumTrips
   TripID = char(tripsSumm{i,11});%Convert java util UUID into char
   Distance = tripsSumm{i,1};
   GenerateScoresEventsV3;
   
   if (Distance < 10)
       AccelScoreV3Bin1 = vertcat(AccelScoreV3Bin1,AccScoreEvtV2);
   else
       AccelScoreV3Bin2 = vertcat(AccelScoreV3Bin2,AccScoreEvtV2);
   end
end

figure 
hist(AccelScoreV3Bin1);
title('Accel Score Hist model V3 , distance<=10km');
ylabel('Occurences');
xlabel('Scores');

figure 
hist(AccelScoreV3Bin2);
title('Accel Score Hist model V3 , distance > 10km');
ylabel('Occurences');
xlabel('Scores');
