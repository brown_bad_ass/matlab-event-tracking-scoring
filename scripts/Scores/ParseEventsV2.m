

%Parse events cell into matrix of duration for different intensities for
%events
EventPkCol = 4;
[EvtRow, EvtCol] = size(Events);


EventTypes = str2num(cell2mat(Events(:,5))); % hack for production

% EventTypes = str2num(cell2mat(Events(:,8))); % hack for staging

%Find number of events of each type so array of that size can be generated
NumAccEvts = sum(EventTypes(:) == 1);
NumBrkEvts = sum(EventTypes(:) == 2);
NumSpdEvts = sum(EventTypes(:) == 3);


DurationBinAccPk = zeros(NumAccEvts,4);
DurationBinBrkPk = zeros(NumBrkEvts,4);
DurationBinSpdPk = zeros(NumSpdEvts,4);

AccCounter = 0;
BrkCounter = 0;

for ind = 1:EvtRow
    
%     EventData = Events{ind,5}; %hack for staging
    
    EventData = Events{ind,8}; %production

    if length(EventData) > 70
        
%     EventType = str2num(cell2mat(Events(ind,8)));
    EventType = str2num(cell2mat(Events(ind,5))); %production
    
    IndexSemiColon = strfind(EventData,':');
    IndexComma = strfind(EventData,',');
    
    Bin1 = EventData(IndexSemiColon(1)+1:IndexComma(1)-1);
    Bin2 = EventData(IndexSemiColon(3)+1:IndexComma(3)-1);
    Bin3 = EventData(IndexSemiColon(5)+1:IndexComma(5)-1);
    EventPeak = Events{ind,EventPkCol};
    
    
    if EventType == 1
        AccCounter = AccCounter + 1;
        temp = [str2num(Bin1) str2num(Bin2) str2num(Bin3) EventPeak ];  
%         DurationBinAccPk =  vertcat(DurationBinAccPk,temp);

        DurationBinAccPk(AccCounter,1) = str2num(Bin1);
        DurationBinAccPk(AccCounter,2) = str2num(Bin2);
        DurationBinAccPk(AccCounter,3) = str2num(Bin3);
        DurationBinAccPk(AccCounter,4) = EventPeak;
     
    end
    
        
    if EventType == 2
        
        BrkCounter = BrkCounter+1;
        
        temp = [str2num(Bin1) str2num(Bin2) str2num(Bin3) EventPeak];  
%         DurationBinBrkPk =  vertcat(DurationBinBrkPk,temp);
         DurationBinBrkPk(BrkCounter,:) = temp;
    end
    


    end
    
    
end

